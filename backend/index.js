// importation des modules

import express from 'express'
import mongoose from 'mongoose'
import cors from 'cors'

//configuragtion express

const app = express()
app.use(express.json())
app.use(cors())

const PORT = process.env.PORT || 8000

// definition du schemma

const schemaData = mongoose.Schema({
    nom: String,
    prenoms:String,
    user:String,
    age:Number,
    date:Date
})

//definition du modele

const userModel = mongoose.model("user",schemaData)


app.get("/",async (req,res)=>{
    try {
        // const id = req.params.id
        const data = await userModel.find({})
        res.json({success : true , data : data})
    } catch (error) {
        console.error("Erreur lors de l'affichage")
    }
})
//app get id

app.get("/:id",async (req,res)=>{
try {
    // const id = req.params.id
    const data = await userModel.findById(id)
    res.json({success : true , data : data})
} catch (error) {
    console.error("Erreur lors de l'affichage")
}

    
})

// app post

app.post("/create",async (req,res)=>{
    try {
        const data = new userModel(req.body)
    await data.save()
    res.json({success : true , message :"Ajout reussis", data : data})
    } catch (error) {
        console.error("Erreur de l'ajout ", error)
    }  
})

// update

app.put("/update/:id",async (req,res)=>{
   
     try {
        const id = req.params.id
        const newData = req.body
        const data = await userModel.findByIdAndUpdate(id,newData,{new : true})
        res.json({succes : true , message: "Données modifier", data : data})
     } catch (error) {
        console.error("Erreur de la modification", error)
     }
    
})

// delete

app.delete("/delete/:id",async (req,res)=>{
    try{
        const id = req.params.id
        const data = await userModel.deleteOne({_id:id})
        res.json({success : true , message : "Données supprimer", data : data})
    }
    catch(error){
        console.error("Erreur lors de la suppression", error)
    }
    
})

//connexion base de données

mongoose.connect("mongodb://localhost:27017/usercrud")
.then(()=>{
    app.listen(PORT,()=>console.log("Connexion reussie"))
    console.log("Connexion port 8000 ")
})
.catch((err)=>console.log(err))