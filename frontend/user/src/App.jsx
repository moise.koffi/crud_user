import { useState, useEffect } from 'react';
import Button from 'react-bootstrap/Button';
import Table from 'react-bootstrap/Table';
import Form from 'react-bootstrap/Form';
import Modal from 'react-bootstrap/Modal';
import axios from 'axios';

function App() {
  axios.defaults.baseURL = 'http://localhost:8000/';

  const [show, setShow] = useState(false);
  const [dataList, setDataList] = useState([]);
  const [selectedUser, setSelectedUser] = useState(null);
  const [formData, setFormData] = useState({
    nom: '',
    prenoms: '',
    user: '',
    age: '',
    date: ''
  });

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    try {
      const response = await axios.get('/');
      setDataList(response.data.data);
    } catch (error) {
      console.error('Erreur lors de la récupération des données:', error);
    }
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const response = await axios.post('/create', formData);
      console.log(response.data);
      handleClose();
      fetchData();
    } catch (error) {
      console.error('Erreur lors de l\'ajout de l\'étudiant:', error);
    }
  };

  const handleUpdate = async () => {
    try {
      const response = await axios.put(`/update/${selectedUser._id}`, formData);
      console.log(response.data);
      handleClose();
      fetchData();
    } catch (error) {
      console.error('Erreur lors de la mise à jour de l\'étudiant:', error);
    }
  };

  const handleDelete = async (id) => {
    try {
      const response = await axios.delete(`/delete/${id}`);
      console.log(response.data);
      fetchData();
    } catch (error) {
      console.error('Erreur lors de la suppression de l\'étudiant:', error);
    }
  };

  const handleClose = () => {
    setShow(false);
    setSelectedUser(null);
    setFormData({
      nom: '',
      prenoms: '',
      user: '',
      age: '',
      date: ''
    });
  };

  const handleShow = () => setShow(true);

  const handleEdit = (user) => {
    setSelectedUser(user);
    setFormData(user);
    setShow(true);
  };

  return (
    <>
      <Button variant="primary" onClick={handleShow}>
        Ajouter un utilisateur
      </Button>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>{selectedUser ? 'Modifier utilisateur' : 'Ajouter utilisateur'}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form onSubmit={selectedUser ? handleUpdate : handleSubmit}>
            <Form.Group className="mb-3">
              <Form.Label>Nom</Form.Label>
              <Form.Control type="text" name="nom" value={formData.nom} onChange={(e) => setFormData({ ...formData, nom: e.target.value })} />
            </Form.Group>
            <Form.Group className="mb-3">
              <Form.Label>Prénoms</Form.Label>
              <Form.Control type="text" name="prenoms" value={formData.prenoms} onChange={(e) => setFormData({ ...formData, prenoms: e.target.value })} />
            </Form.Group>
            <Form.Group className="mb-3">
              <Form.Label>Utilisateur</Form.Label>
              <Form.Control type="text" name="user" value={formData.user} onChange={(e) => setFormData({ ...formData, user: e.target.value })} />
            </Form.Group>
            <Form.Group className="mb-3">
              <Form.Label>Age</Form.Label>
              <Form.Control type="number" name="age" value={formData.age} onChange={(e) => setFormData({ ...formData, age: e.target.value })} />
            </Form.Group>
            <Form.Group className="mb-3">
              <Form.Label>Date</Form.Label>
              <Form.Control type="date" name="date" value={formData.date} onChange={(e) => setFormData({ ...formData, date: e.target.value })} />
            </Form.Group>
            <Button variant="secondary" onClick={handleClose}>
              Annuler
            </Button>
            <Button variant="primary" type="submit">
              {selectedUser ? 'Modifier' : 'Ajouter'}
            </Button>
          </Form>
        </Modal.Body>
      </Modal>

      <Table striped bordered hover>
        <thead>
          <tr>
            <th>#</th>
            <th>Nom</th>
            <th>Prénoms</th>
            <th>Utilisateur</th>
            <th>Age</th>
            <th>Date</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {dataList.map((user, index) => (
            <tr key={user._id}>
              <td>{index + 1}</td>
              <td>{user.nom}</td>
              <td>{user.prenoms}</td>
              <td>{user.user}</td>
              <td>{user.age}</td>
              <td>{user.date}</td>
              <td>
                <Button variant="info" size="sm" onClick={() => handleEdit(user)}>Modifier</Button>{' '}
                <Button variant="danger" size="sm" onClick={() => handleDelete(user._id)}>Supprimer</Button>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </>
  );
}

export default App;
